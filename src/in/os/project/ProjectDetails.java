package in.os.project;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class ProjectDetails {

	/**
	 * Get all projects
	 * 
	 * @return List of projects names configured in the properties file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Set<String> getAllProjects() throws FileNotFoundException, IOException {
		Properties prop = getProjectsDetails();
		return prop.stringPropertyNames();
	}

	/**
	 * get Project URL
	 * 
	 * @param aProject
	 * @return the URL for the given project
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public String getProjectURL(String aProject) throws FileNotFoundException, IOException {
		return getProjectsDetails().getProperty(aProject);
	}

	/**
	 * Return list of all Projects and its URLs
	 * 
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private Properties getProjectsDetails() throws IOException, FileNotFoundException {
		String filename = "projects.properties";
		Properties prop = new Properties();
		try (InputStream propFileInpStream = new FileInputStream("C:\\expws\\JGitExp\\" + filename)) {
			prop.load(propFileInpStream);
		}
		return prop;
	}

	public static void main(String aArgs[]) {
		ProjectDetails pd = new ProjectDetails();
		try {
			for (String project : pd.getAllProjects()) {
				System.out.println(project + " - " + pd.getProjectURL(project));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
