package in.os;

import java.io.IOException;
import java.util.List;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import in.os.jgit.CommitHistory;
import in.os.jgit.JGit;
import in.os.project.ProjectDetails;

public class JGitExp {

	public static void main(String args[]) {

		try {
			ProjectDetails pd = new ProjectDetails();
			String repoName = pd.getProjectURL("angular2");
			repoName = pd.getProjectURL("jgitexp");
			//repoName = pd.getProjectURL("friends");
			//repoName = pd.getProjectURL("nova.module.maintenance");
			//exp.getUserCredentials();
			JGit jgit = new JGit();
			Git git = jgit.createClone(repoName, true);
			List<CommitHistory> historyWalk = jgit.historyWalk(git, 20);
			jgit.printHistory(historyWalk);
			git.close();
		} catch (GitAPIException | IOException e) {
			e.printStackTrace();
		}

	}
}
