package in.os.jgit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class CommitHistory {

	private String id;
	private String description;
	private String user;
	private Date when;
	private String commitName;
	private List<String> commitedFiles = Collections.<String>emptyList();

	public void setID(String aID) {
		this.id = aID;
	}

	public void setMessage(String aDescription) {
		this.description = aDescription;
	}

	public void setUser(String aUser) {
		this.user = aUser;
	}

	public void setWhen(Date aWhen) {
		this.when = aWhen;
	}

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getUser() {
		return user;
	}

	public Date getWhen() {
		return when;
	}

	public String getCommitName() {
		return this.commitName;
	}

	public void setCommitName(String aCommitName) {
		this.commitName = aCommitName;
	}

	public void addCommitedFile(String aFile) {
		if (commitedFiles.isEmpty()) {
			commitedFiles = new ArrayList();
		}
		commitedFiles.add(aFile);
	}

	public List<String> getCommitedFiles() {
		return this.commitedFiles;
	}

	@Override
	public String toString() {
		return this.id + " : " + this.description + " by " + this.user + " at " + this.when + " -> " + this.commitName;
	}

}
