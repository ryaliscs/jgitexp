package in.os.jgit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.errors.AmbiguousObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

public class JGit {

	public UsernamePasswordCredentialsProvider getUserCredentials() {
		return new UsernamePasswordCredentialsProvider("sarat.ryali@planonsoftware.com", "wxyz");
	}

	public void printHistory(List<CommitHistory> aHistoryWalk) {
		for (CommitHistory ch : aHistoryWalk) {
			System.out.println(ch);
			for (String file : ch.getCommitedFiles()) {
				System.out.println(file);
			}
			System.out.println("\n");
		}
	}

	public List<CommitHistory> historyWalk(Git aGit, int aNoOfHistory)
			throws MissingObjectException, IncorrectObjectTypeException, IOException, NoHeadException, GitAPIException {
		List<CommitHistory> lch = new ArrayList<CommitHistory>(aNoOfHistory);
		Repository repository = aGit.getRepository();
		System.out.println("Commits for branch :" + aGit.getRepository().getBranch());
		try (RevWalk revWalk = new RevWalk(repository)) {
			ObjectId commitId = repository.resolve(aGit.getRepository().getBranch());
			if (commitId != null) {
				revWalk.markStart(revWalk.parseCommit(commitId));
				int i = 0;
				for (RevCommit commit : revWalk) {
					if (i++ == (aNoOfHistory + 1)) { 
						// +1 is added to get the committed files for the last requested commit
						break;
					}
					CommitHistory ch = new CommitHistory();
					ch.setID(commit.getId().abbreviate(7).name());
					ch.setMessage(commit.getFullMessage());
					ch.setUser(commit.getAuthorIdent().getName());
					ch.setWhen(commit.getAuthorIdent().getWhen());
					ch.setCommitName(commit.getTree().getName());
					lch.add(ch);
				}
			}
		}
		getCommitedFiles(repository, lch, aNoOfHistory);
		return lch.subList(0, Math.min(lch.size(), aNoOfHistory));
	}

	private void getCommitedFiles(Repository repository, List<CommitHistory> lch, int aNoOfHistory)
			throws RevisionSyntaxException, AmbiguousObjectException, IncorrectObjectTypeException, IOException,
			GitAPIException {
		int size = lch.size();
		for (int i = 0; i < size; i++) {
			CommitHistory current = lch.get(i);
			ObjectId oldHead = repository.resolve(current.getCommitName());
			ObjectId head;
			if ((i + 1) == size) {
				//if (size <= aNoOfHistory || size > aNoOfHistory) {
					head = repository.resolve("HEAD~^{tree}");
//				} 
//				else {
//					CommitHistory previous = lch.get(i + 1);
//					head = repository.resolve(previous.getCommitName());
//				}

			} else {
				CommitHistory previous = lch.get(i + 1);
				head = repository.resolve(previous.getCommitName());
			}

			// System.out.println("Printing diff between tree: " + oldHead + "
			// and " + head);

			// prepare the two iterators to compute the diff between
			try (ObjectReader reader = repository.newObjectReader()) {
				CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
				oldTreeIter.reset(reader, oldHead);
				CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
				newTreeIter.reset(reader, head);

				// finally get the list of changed files
				try (Git git = new Git(repository)) {
					List<DiffEntry> diffs = git.diff().setNewTree(newTreeIter).setOldTree(oldTreeIter).call();
					for (DiffEntry entry : diffs) {
						// System.out.println("Entry: " + entry.getOldPath());
						current.addCommitedFile(entry.getOldPath());
					}
				}
			}
		}
	}

	public Git createClone(String aUrl)
			throws InvalidRemoteException, TransportException, GitAPIException, IOException {
		return this.createClone(aUrl,true);
	}
	public Git createClone(String aUrl, boolean aUpdate)
			throws InvalidRemoteException, TransportException, GitAPIException, IOException {
		String repoName = aUrl.substring(aUrl.lastIndexOf("/") + 1, aUrl.lastIndexOf("."));
		System.out.println(repoName);
		String path = "/temp/path/to/" + repoName;
		File cloneFolder = new File(path);
		path += "/.git";
		if (cloneFolder.exists()) {
			Repository repo;
			if (aUpdate) {
				System.out.println("Git Pull");
				cloneFolder = new File(path);
				repo = new FileRepository(cloneFolder);
				Git git = new Git(repo);
				git.pull().call();
				return git;	
			} else {
				System.out.println("Open an existing repository");
				repo = new FileRepositoryBuilder().setGitDir(new File(path)).build();
				return new Git(repo);
			}
			
		} else {
			System.out.println("Git clone");
			CloneCommand cloneCommand = Git.cloneRepository();
			cloneCommand = cloneCommand.setURI(aUrl);
			return cloneCommand.setDirectory(cloneFolder).call();
		}

	}
}
