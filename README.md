# README #

### What is this repository for? ###

* This repository is used to clone an existing git repo and get the history of the repo
* 1,0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* This is a simple java project
* Dependencies: (refer http://download.eclipse.org/jgit/site/4.4.1.201607150455-r/org.eclipse.jgit/dependencies.html)
# org.eclipse.jgit-4.7.0.201704051617-r.jar
# jsch-0.1.54.jar
# slf4j-api-1.7.25.jar

Other references:
http://www.codeaffine.com/2015/12/15/getting-started-with-jgit/
http://wiki.eclipse.org/JGit/User_Guide#RevWalk
credential provider example
https://github.com/warmuuh/pactbroker-maven-plugin/issues/4 

### Who do I talk to? ###

* R Sarat Chandra Sekhar (ryaliscs@gmail.com)